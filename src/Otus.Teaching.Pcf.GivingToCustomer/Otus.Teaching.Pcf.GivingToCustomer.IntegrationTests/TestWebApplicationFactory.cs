﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests.Fakes;

namespace Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests
{
    public class TestWebApplicationFactory<TStartup>
        : WebApplicationFactory<TStartup> where TStartup: class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                services.AddScoped<INotificationGateway, FakeNotificationGateway>();
                /*
                var sp = services.BuildServiceProvider();

                using var scope = sp.CreateScope();
                var scopedServices = scope.ServiceProvider;
                var preferenceCollection = scopedServices.GetRequiredService<IMongoCollection<Preference>>();
                var customerCollection = scopedServices.GetRequiredService<IMongoCollection<Customer>>();
                var promoCodeCollection = scopedServices.GetRequiredService<IMongoCollection<PromoCode>>();
                var logger = scopedServices
                    .GetRequiredService<ILogger<TestWebApplicationFactory<TStartup>>>();
                
                try
                {
                    new MongoTestDbInitializer(preferenceCollection, customerCollection, promoCodeCollection)
                        .InitializeDb();
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Проблема во время заполнения тестовой базы. " +
                                        "Ошибка: {Message}", ex.Message);
                }
                */
            });
        }
    }
}